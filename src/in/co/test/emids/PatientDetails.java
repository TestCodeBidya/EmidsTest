package in.co.test.emids;

public class PatientDetails {
	private String name;
	private int age;
	private String gender;
	private String hypertension;
	private String bloodPressure;
	private String boodSugar;
	private String overWeight;
	private String smoking;
	private String alcohol;
	private String dailyExercise;
	private String drugs;
	
	public PatientDetails(String name, int age, String gender,
			String hypertension, String bloodPressure, String boodSugar,
			String overWeight, String smoking, String alcohol,
			String dailyExercise, String drugs) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.boodSugar = boodSugar;
		this.overWeight = overWeight;
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.dailyExercise = dailyExercise;
		this.drugs = drugs;
	}

	public String toString() {
		return "PatientDetails [name=" + name + ", age=" + age + ", gender="
				+ gender + ", hypertension=" + hypertension
				+ ", bloodPressure=" + bloodPressure + ", boodSugar="
				+ boodSugar + ", overWeight=" + overWeight + ", smoking="
				+ smoking + ", alcohol=" + alcohol + ", dailyExercise="
				+ dailyExercise + ", drugs=" + drugs + "]";
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getGender() {
		return gender;
	}

	public String getHypertension() {
		return hypertension;
	}

	public String getBloodPressure() {
		return bloodPressure;
	}

	public String getBoodSugar() {
		return boodSugar;
	}

	public String getOverWeight() {
		return overWeight;
	}

	public String getSmoking() {
		return smoking;
	}

	public String getAlcohol() {
		return alcohol;
	}

	public String getDailyExercise() {
		return dailyExercise;
	}

	public String getDrugs() {
		return drugs;
	}

	
	
	
	

}
